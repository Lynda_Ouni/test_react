import React, { Component } from 'react';

class ProjectItem extends Component {
  //delete methode 
  deleteProject(id){
    this.props.onDelete(id);
  }

  render() {
    //outputing all our projects along with category
    //add event for deleting objects with id
    //delete methode for current project
    return (
      <li className="Project">
        <strong>{this.props.project.title}</strong>: {this.props.project.category} 
        <button onClick={this.deleteProject.bind(this, this.props.project.id)}>X</button>
      
      </li>
    );
  }
}
//validation of Proprieties types
ProjectItem.propTypes = {
  project: React.PropTypes.object,
  onDelete: React.PropTypes.func
}

export default ProjectItem;
